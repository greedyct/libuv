#ifndef CORE_H
#define CORE_H

#include <sys/socket.h>
#include <sys/epoll.h>
#include <vector>
#include "list.h"
#include "array.h"

class EventLoop;
class IoEvent;
class Handle;
class Stream;
class ReqBase;
class WriteReq;
class ConnectReq;
class ShutdownReq;

typedef struct iovec Buf;

//socket
int NewSocket();
int SetNonblock(int fd);
int SetCloexec(int fd);


//callback
typedef void (*io_cb)(EventLoop *loop, IoEvent *ev, unsigned int events);
typedef void (*write_cb)(WriteReq *req, int status);
typedef void (*shutdown_cb)(ShutdownReq *req, int status);
typedef void (*stream_connect_cb)(Stream *stream, int status);
typedef void (*handle_close_cb)(Handle *handle, int status);
typedef void (*alloc_cb)(Stream *stream, Buf *buf, int buf_size);
typedef void (*read_cb)(Stream *stream, Buf *buf, int nread);

//
enum {
    Kwrite,
    Kread,
};

class EventLoop {
public:
    EventLoop();
    ~EventLoop();
    void IoStart(IoEvent *e, unsigned int events);
    void IoStop(IoEvent *e, unsigned int events);
    void Run();
    void StopLoop();

private:
    void IoPoll(int timeout);
    void ModifyEpollEv();

public:
    int epollfd_;
    Array<IoEvent *> watchers_;
    ListNode watch_queue_;
    ListNode handle_queue_;
    std::vector<struct epoll_event> epoll_events_;
    ListNode active_req_queue;
};


class ReqBase {
public:
    ReqBase() : data_(NULL), req_type_(-1) {
        ListInit(&active_queue_);
        active_queue_.ptr = this;
    }
    void *data_;
    int req_type_;
    ListNode active_queue_;
};

class WriteReq : public ReqBase {
public:
    WriteReq() : cb_(NULL),
                 send_handle_(NULL),
                 handle_(NULL),
                 bufs_(NULL),
                 write_index_(0),
                 error_(0) {
        ListInit(&queue_);
    }
    write_cb cb_;
    Stream* send_handle_;
    Stream* handle_;
    ListNode queue_;
    unsigned int write_index_;
    Buf* bufs_;
    Buf bufsml_[4];
    int error_;
    unsigned int nbufs_;
};

class ShutdownReq : public ReqBase {
public:
    shutdown_cb cb_;
    Stream* handle_;
};

class ConnectReq : public ReqBase {
public:
    stream_connect_cb cb_;
    Stream* handle_;
    ListNode queue_;
};


class IoEvent {
public:
    IoEvent(io_cb cb, int fd);
public:
    io_cb cb_;
    ListNode watch_queue_;
    int fd_;
    unsigned int events_;
    unsigned int pevents_;
};

IoEvent::IoEvent(io_cb cb, int fd) {
    cb_ = cb;
    fd_ = fd;
    events_ = 0;
    pevents_ = 0;
    ListInit(&watch_queue_);
    watch_queue_.ptr = this;
}

class Handle {
public:
    Handle(EventLoop *loop, int type) : loop_(loop), type_(type) {
        ListTailAdd(&loop_->handle_queue_, &handle_queue_);
    }

    void *data_;
    EventLoop *loop_;
    int type_;
    int flags_;
    ListNode handle_queue_;
    handle_close_cb cb_;
};

enum {
    K_STREAM_READING,
    K_STREAM_READ_PARTIAL,
    K_STREAM_READ_EOF
}

class Stream : public Handle {
public:
    Stream(EventLoop *loop, int type);
    ~Stream();
    int Write(WriteReq *req, Stream* send_handle, Buf bufs[], unsigned int niovec, write_cb cb);
    int IsReadable();
    int IsWriteable();
    static void ServerIo(EventLoop *loop, IoEvent *ev, unsigned int events);
    static void StreamIo(EventLoop *loop, IoEvent *ev, unsigned int events);
    void Shutdown();
    void Open(int fd, int flags);
    void WriteReqFinish(WriteReq *req);
    void StreamReadEof();

private:
    void Write_();
    void Read_();
public:
    ListNode write_queue_;
    ListNode write_completed_queue_;
    ShutdownReq* shutdown_req_;
    ConnectReq* connect_req_;
    int write_queue_size_;
    int delayed_error_;
    IoEvent io_watcher_;
    read_cb read_cb_;
    alloc_cb alloc_cb_;
};

#endif
