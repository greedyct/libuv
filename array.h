#ifndef ARRAY_H
#define ARRAY_H
#include <malloc.h>

template<typename T>
class Array {
public:
    Array() : size(0),
              cap(16),
              array( (T*)malloc(cap * sizeof(T)) ) {}

    ~Array() {
        free(array);
    }


    unsigned int Size() {
        return size;
    }

    inline T& operator[] (int index) {
        while(index >= cap) {
            array = (T*)realloc(array, 2 * cap * sizeof(T));
            for(int i = cap; i < 2 * cap; i++) {
                array[i] = NULL;
            }
            cap *= 2;
        }
        return array[index];
    }


private:
    unsigned int size;
    unsigned int cap;
    T* array;
};

#endif
