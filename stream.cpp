#include "core.h"
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <sys/poll.h>
#include "list.h"
#include "common.h"

Stream::Stream(EventLoop *loop, int type) :
    Handle(loop, type),
    shutdown_req_(NULL),
    connect_req_(NULL),
    write_queue_size_(0),
    delayed_error_(0),
    io_watcher_(StreamIo, -1)
{
    ListInit(&write_queue_);
    ListInit(&write_completed_queue_);
}

Stream::~Stream() {

}

void Stream::StreamIo(EventLoop *loop, IoEvent *ev, unsigned int events) {
}

void Stream::ServerIo(EventLoop *loop, IoEvent *ev, unsigned int events) {
}

void Stream::Write_() {
    assert(io_watcher_.fd_ > 0);

    if(ListEmpty(&write_queue_)) {
        return;
    }

    ListNode *p = write_queue_.next;
    WriteReq *req = (WriteReq *)p->ptr;
    int nwrite = 0;

    struct iovec* iov = req->bufs_;
    int iovcnt = req->nbufs_ - req->write_index_;

    if(req->send_handle_) {
        //TO-DO
    } else {
        //调用writev
        do {
            nwrite = writev(io_watcher_.fd_, iov, iovcnt);
        }
        while(nwrite == -1 && errno == EINTR);

        if(nwrite < 0) {
            if(errno != EAGAIN && errno != EWOULDBLOCK) {
                req->error_ = -errno;
                WriteReqFinish(req);
                loop_->IoStop(&io_watcher_, POLLOUT);
                if(io_watcher_.pevents_ | POLLIN) {
                    //TO-DO stop handle
                }
                return;
            }
        } else {
            //写入了数据
            while(nwrite >= 0) {
                Buf *buf = &req->bufs_[req->write_index_];
                int len = buf->iov_len;
                if(nwrite < len) {
                    write_queue_size_ -= nwrite;
                    buf += nwrite;
                    len -= nwrite;
                    nwrite = 0;
                    break;
                } else {
                    req->write_index_++;
                    nwrite -= len;
                    write_queue_size_ -= len;

                    if(req->write_index_ == req->nbufs_) {
                        WriteReqFinish(req);
                        return;
                    }
                }
            }
        }
    }
    //1.从break跳出来,由于tcp缓冲buffer满了只写入了部分，需要注册可写事件，等可写的时候写入
    //2.从EAGAIN跳出来
    loop_->IoStart(&io_watcher_, POLLOUT);
}

void Stream::Read_() {
    Buf buf;
    flags_ |= ~K_STREAM_READ_PARTIAL;

    while(alloc_cb_ && (flags_ & K_STREAM_READING) ) {
        
    }
}


int Stream::Write(WriteReq *req, Stream* send_handle, Buf bufs[], unsigned int nbufs, write_cb cb) {
    assert(nbufs > 0);

    if(io_watcher_.fd_ < 0) {
        return -EBADF;
    }
    bool queue_empty = (write_queue_size_ == 0);

    //加入loop的活跃队列
    req->send_handle_ = send_handle;
    req->handle_ = this;
    req->cb_ = cb;
    ListTailAdd(&loop_->active_req_queue, &req->active_queue_);

    req->bufs_ = &req->bufsml_[4];
    if(nbufs > 4) {
        //这里是否可以用 std::move
        req->bufs_ = new Buf[nbufs];
    }

    memcpy(req->bufs_, bufs, nbufs * sizeof(Buf));
    req->nbufs_ = nbufs;
    req->write_index_ = 0;
    write_queue_size_ += BufCount(req->bufs_, req->nbufs_);

    ListTailAdd(&write_queue_, &req->queue_);
    if(connect_req_ != NULL) {
        //啥都不做 
    } else if(queue_empty) {
        Write_();
    } else {
        loop_->IoStart(&io_watcher_, POLLOUT);
    }
    return 0;
}

void Stream::WriteReqFinish(WriteReq *req) {
       
}

void Stream::Shutdown() {
}

void Stream::Open(int fd, int flags) {
}
