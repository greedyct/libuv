#include "core.h"
#include "list.h"
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>


int NewSocket() {
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
}

int SetNonblock(int fd) {
    int r;
    do {
        r = fcntl(fd, F_GETFL);
    }while(r < 0 && errno == EINTR);

    do
        r = fcntl(fd, F_SETFL, r | O_NONBLOCK);
    while (r == -1 && errno == EINTR);
    return r;
}

int SetCloexec(int fd) {
    int r;
    do {
        r = fcntl(fd, F_GETFL);
    }while(r < 0 && errno == EINTR);

    do
        r = fcntl(fd, F_SETFL, r | FD_CLOEXEC);
    while (r == -1 && errno == EINTR);
    return r;
}

EventLoop::EventLoop() : epollfd_(0), epoll_events_(256) {
    ListInit(&watch_queue_);
    epollfd_ = epoll_create1(0);
    if (epollfd_ == -1) {
        perror("epoll_create1");
        exit(0);
    }
}

EventLoop::~EventLoop() {

}

void EventLoop::IoStart(IoEvent *e, unsigned int events) {
    assert(events != 0);
    e->pevents_ |= events;

    if(events == e->pevents_) {
        return;
    }

    if(watchers_[e->fd_] == NULL) {
        watchers_[e->fd_] = e;
    }

    if(ListEmpty(&e->watch_queue_)) {
        ListTailAdd(&watch_queue_, &e->watch_queue_);
    }
}

void EventLoop::IoStop(IoEvent *e, unsigned int events) {
    assert(events != 0);
    e->pevents_ &= ~events;
    if(e->pevents_ == 0) {
        ListDel(&e->watch_queue_);
        ListInit(&e->watch_queue_);
        watchers_[e->fd_] = NULL;
    }
}

void EventLoop::ModifyEpollEv() {
    int op;
    struct epoll_event ep;
    ListNode *queue;
    IoEvent *ev;
    while(ListEmpty(&watch_queue_)) {
        queue = watch_queue_.next;
        ListDel(queue);
        ListInit(queue);
        //ev = ContainOf(queue, IoEvent, watch_queue_);
        ev = (IoEvent *)(queue->ptr);
        ep.events = ev->events_;
        ep.data.fd = ev->fd_;

        op = ep.events == 0 ? EPOLL_CTL_ADD : EPOLL_CTL_MOD;
        if(epoll_ctl(epollfd_, op, ev->fd_, &ep)) {
            if(errno != EEXIST) {
                exit(-1);
            }

            if(epoll_ctl(epollfd_, EPOLL_CTL_MOD, ev->fd_, &ep)) {
                perror("epoll_ctl error");
                exit(-1);
            }
        }
        ev->events_ = ev->pevents_;
    }
}

void EventLoop::IoPoll(int timeout) {
    struct epoll_event ep;
    IoEvent *ev;
    int nfds;
    int want;

    ModifyEpollEv();

    for (;;) {
        nfds = epoll_wait(epollfd_, &*epoll_events_.begin(), epoll_events_.size(), timeout);
        if (nfds == -1) {
            perror("epoll_wait");
            exit(EXIT_FAILURE);
        }

        for (int i = 0; i < nfds; ++i) {
            int fd = epoll_events_[i].data.fd;
            ev = watchers_[fd];
            if(ev == NULL) {
                epoll_ctl(epollfd_, EPOLL_CTL_DEL, fd, &epoll_events_[i]);
            }

            //此轮发生的关注的事件
            want = epoll_events_[i].events & (ev->pevents_ | EPOLLERR | EPOLLHUP);
            //hup和err不管,让read/write处理
            if(want == EPOLLERR || want == EPOLLHUP) {
                want |= ev->pevents_ & (EPOLLIN | EPOLLOUT);
            }
            ev->cb_(this, ev, want);
        }

        if(epoll_events_.size() == nfds) {
            epoll_events_.resize(epoll_events_.size() * 2);
        }
    }
}

void EventLoop::Run() {

}
