#include "core.h"

static inline int BufCount(Buf *bufs, unsigned int nbufs) {
    size_t size = 0;
    for(int i = 0; i < nbufs; i++) {
        size += bufs[i].iov_len;
    }
    return size;
}
