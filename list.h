#ifndef ListNode_H
#define ListNode_H
#include <stddef.h>

struct ListNode {
    ListNode() : next(NULL), prev(NULL), ptr(NULL) {}
    ListNode *prev, *next;
    void *ptr;
};



static inline void ListInit(ListNode *l) {
    l->prev = l;
    l->next = l;
}

static inline bool ListEmpty(ListNode *l) {
    return l->prev == l && l->next == l;
}

static inline void ListTailAdd(ListNode *h, ListNode *p) {
    p->next = h;
    p->prev = h->prev;
    h->prev->next = p;
    h->prev = p;
}

static inline void ListHeadAdd(ListNode *h, ListNode *p) {
    p->next = h->next;
    p->prev = h;
    h->next->prev = p;
    h->next = p;
}

static inline void ListDel(ListNode *p) {
    p->prev->next = p->next;
    p->next->prev = p->prev;
}

#endif
